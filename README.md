<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->

<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  Gatsby's hello-world starter with styled components
</h1>

*Note that this project is just here to share my first experiences with using Gatsby. This project is not intended as a useful starter, although if you think this is any useful, feel free to have a look. Otherwise, have another more specific idea? You may want to check out our vibrant collection of [official and community-created starters](https://www.gatsbyjs.org/docs/gatsby-starters/).*

Please note

Additions to the original `hello world` project from Gatsby:

- Use styled components instead,
- Navigation bar (non-reponsive, it's just there..),
- Additional styling on 404 page,
- Add a navigation bar to link to the prev and next posts, inspired from [tayiorbeii](https://github.com/tayiorbeii/gatsby-demo-blog-code).

To do:

- [ ] Make header bar full width and responsive
- [ ] Make footer bar full width and responsive
- [ ] Add logo
- [ ] Style nav items
- [ ] Improve home page with full width image
- [ ] Style article preview (date smaller, italic on the right, spacing etc)
- [ ] Write about page
- [ ] Write contact page
- [ ] Write first article
- [ ] Deploy

Later:

- [ ] Sendgrid integration
- [ ] Check image styling
- [ ] Styling the 404 page.

**`LICENSE`**: Gatsby is licensed under the MIT license.
