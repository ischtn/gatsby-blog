---
title: "Deviterate"
date: "2018-09-01"
status: Published
excerpt: "Progressive webapp in ES6 vanilla Javascript"
keywords: "CSS, ES6, Web"
featuredImage: "./deviterate.png"
link: "https://deviterate.com"
---
