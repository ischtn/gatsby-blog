---
title: "Gatsby portfolio and Blog"
date: "2019-05-10"
status: Published
excerpt: "Blog and portfolio website created in Gatsby."
keywords: "css, gatsbyjs, react, styled-components"
featuredImage: "./indexHeader.jpg"
---


![Happy-stack](./happy-stack.png)

## The why

My previous was based on a Bootstrap theme created in a bit of a hurry during a job search about a year back. Like the site you are viewing now, it was a purely a frontend website without any server side code.

Although I think it was 'mostly fine', I decided that it was time to move away from a stack consisting of Jquery,Bootstrap and Sass and do things differently.

During the past year, I got the chance to gain a little experience with React and I decided I wanted my new website to be sort of representing my current perspective of 'good' webdesign.


## The how

This website was created using one of 2018's buzzwords `JAMstack` (Short for the combination of *'Javascript, APIs and Markup '*).

In this case, that Jam stack is composed of:
- [GatsbyJS](https://www.gatsbyjs.org/)  (an awesome tool based on ReactJS 💜),
- which uses [GraphQL](https://graphql.com) to generate static, 'blazingly fast' websites,
- in combination with [styled components](https://www.styled-components.com/) 💅 for markup (which, although I'm still figuring it out, is such a nice and clean way of styling websites),
- hosting on [Netlify](https://netlify.com) for allowing server side functionalities where necessary,
- and currently using Markdown to actually write content.

## Hosting

I used to serve my portfolio website from an S3 bucket on AWS and automatically deployed everytime I pushed something to Gitlab. (which is still a great solution by the way!).
But, although a pure frontend website is great for a small portfolio/simple blog, one of their challenges is that some things, simply cannot (or should not) be done without some server(less) components.

A nice example is a contact form, and I never spend the time to set that up on my old website.

Turned out, I did not have to for my new one either!
After hearing a lot of good stories about [Netlify](https://netlify.com) I tried it out, and amazingly, that contact form was basically working on the first try!

So, although I spent most of my coding-time in python the past few years, I've concluded that frontend development in 2019 can really be great when you're allowed to choose a set of nice tools.
Things may not be perfect yet, but its quite easy to feel spoiled as a developer now...

# More info

Of course..you're already on this website.
But if you would like to have a look under the hood, feel free to check it out on [Gitlab](https://gitlab.com/ischouten/gatsby-blog).

![Site](site.png)