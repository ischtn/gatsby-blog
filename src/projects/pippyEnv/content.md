---
title: "pipPyEnv"
date: "2019-04-19"
excerpt: "WSL setup script for pyenv with pipenv"
keywords: "bash, ubuntu, devTools"
status: Published
featuredImage: "./pippyenv.png"
link: "https://github.com/ischouten/pipPyEnv"
---
