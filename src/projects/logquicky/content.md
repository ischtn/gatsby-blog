---
title: "Logquicky"
date: "2019-01-31"
status: Published
keywords: "python, PyPi, devTool"
link: "https://pypi.org/project/logquicky"
excerpt: "Quickly get nice looking logging. Published on PyPi"
featuredImage: "./logquicky.png"
---
