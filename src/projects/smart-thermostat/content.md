---
title: "Smart thermostat diagnostics"
date: "2018-12-19"
featuredImage: "./plugwise.jpg"
excerpt: "Remote diagnostics for Smart Thermostat"
keywords: "python, matplotlib, devTool"
status: Published
---

 [Plugwise](https://plugwise.com) offers both a Smart thermostat (Anna) as well as so called 'zone-controllers' and TRVs (thermostatic radiator valves.
During my time there, especially during the last few years - there was a need to deeply analyse the behavior of our products such as the Anna smart Thermostat.

Therefore I created an application in python which could be used to pull data from its local (xml based) REST API and visualize various types of technical information that was stored on Anna itself.
Think like information such as temperatures of the central heating system, and those of the different zones in the building as well as error codes.

Once customers requested assistance because they needed some help or had questions about the heating configuration in their home they could enable us to setup a connection to their system and analyse the data (with their consent of course).

The application was created in python3, using - most importantly - matplotlib, numpy and scipy.

![Charts](./pylogplot.jpg)
24 hour chart of smart thermostat and a boiler

![Single zone plot](./single-zone-plot.png)
Single 'zone' view, controlled by different TRVs and room-controllers.

You can read more about Anna and Plugwise's smart heating solutions [on their website](https://plugwise.com).

And here is the original introduction video from when Plugwise released the Anna Smart thermostat back in 2014...

<center>
  <iframe width="560" height="315" src="https://www.youtube.com/embed/WZnrY25-eNg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>