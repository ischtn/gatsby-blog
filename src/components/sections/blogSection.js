// TODO: Add react query
import React from "react";
import styled from "styled-components";
import Zoom from "react-reveal/Zoom";
import withReveal from "react-reveal/withReveal";
import Title from "./sectionTitle";
import { StaticQuery, Link, graphql } from "gatsby";
import SectionContent from "./sectionContent";
import Img from "gatsby-image";

const SectionBody = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  flex-flow: row wrap;
  width: 100%;

  h3 {
    width: 100%;
    flex-base: 100%;
    text-align: center;
  }
`;

const ProjectCard = withReveal(
  styled.div`
    width: 200px;
    margin: 2rem;
    box-radius: 5px;
    border: solid 1px var(--light-gray);
  `,
  <Zoom cascade />
);

// Gatsby automagically maps the query data into data, so that it can be used here.
export default () => (
  <StaticQuery
    query={graphql`
      {
        allMarkdownRemark(
          filter: { fileAbsolutePath: { regex: "/(blog)/" } }
          limit: 1
        ) {
          edges {
            node {
              id
              fields {
                slug
              }
              frontmatter {
                title
                date(formatString: "MMMM Do, YYYY")
                featuredImage {
                  id
                  childImageSharp {
                    id
                    sizes(maxWidth: 300) {
                      ...GatsbyImageSharpSizes
                    }
                  }
                }
              }
              excerpt
            }
          }
        }
      }
    `}
    render={(data) => (
      <div className="section bg-accent">
        <SectionContent>
          <Title id="blog">Latest blog post</Title>
          <SectionBody>
            {data.allMarkdownRemark &&
              data.allMarkdownRemark.edges.map(({ node }, index) => {
                return (
                  <ProjectCard key={node.id}>
                    <Link to={node.fields.slug}>
                      <div>
                        <Img
                          sizes={
                            node.frontmatter.featuredImage.childImageSharp.sizes
                          }
                        />
                        <div>
                          <h3>{node.frontmatter.title}</h3>
                        </div>
                      </div>
                    </Link>
                  </ProjectCard>
                );
              })}
          </SectionBody>
        </SectionContent>
      </div>
    )}
  />
);
