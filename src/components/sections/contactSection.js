import React from "react";
import styled from "styled-components";
import Zoom from "react-reveal/Zoom";
import Fade from "react-reveal/Fade";
import Title from "./sectionTitle";
import SectionContent from "./sectionContent";

const SectionBody = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  flex-flow: column wrap;
  width: 60%;
  max-width: 80%;

  h3 {
    width: 100%;
    flex-base: 100%;
    text-align: center;
  }

  @media (max-width: 960px) {
    width: 100%;
    max-width: 80vmin;
  }
`;

const FormRow = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  flex-flow: row wrap;
  margin: 1rem 0;

  input,
  textarea {
    width: 70%;
    padding: 5px;
    border: solid 1px var(--light-gray);
  }

  input[type="submit"] {
    width: 10rem;
    height: 2rem;
    margin-left: 50%;
    transform: translateX(-50%);
    border-radius: 0.5rem;
    background-color: white;
    border: solid 1px var(--light-gray);
    text-align: center;
    color: var(--dark-gray);
    padding: 5px;

    :hover {
      background-color: var(--accent-color);
      cursor: pointer;
      color: white;
    }
  }
`;

const ContactSection = () => {
  return (
    <div className="section">
      <SectionContent>
        <Title id="contact">Contact me</Title>
        <SectionBody>
          <Fade right>
            <h3>Want to get into contact? Drop me an email!</h3>
          </Fade>

          <form
            name="contact"
            method="post"
            data-netlify="true"
            data-netlify-honeypot="bot-field"
          >
            <input type="hidden" name="bot-field" />
            <input type="hidden" name="form-name" value="contact" />
            <Zoom>
              <FormRow>
                <label htmlFor="name">Name</label>
                <input type="text" name="name" id="name" />
              </FormRow>
            </Zoom>
            <Zoom>
              <FormRow>
                <label htmlFor="email">Email</label>
                <input type="text" name="email" id="email" />
              </FormRow>
            </Zoom>
            <Zoom>
              <FormRow>
                <label htmlFor="message">Message</label>
                <textarea name="message" id="message" rows="8" />
              </FormRow>
            </Zoom>
            <Zoom>
              <FormRow className="centered">
                <input type="submit" value="Send message" />
              </FormRow>
            </Zoom>
          </form>
        </SectionBody>
      </SectionContent>
    </div>
  );
};

export default ContactSection;
