import styled from "styled-components";

const SectionContent = styled.div`
  display: flex;
  flex-flow: column wrap;
  justify-content: center;
  align-items: center;
`;

export default SectionContent;
