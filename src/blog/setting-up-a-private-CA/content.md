---
title: "Setting up a private CA"
date: "2019-01-31"
status: Published
featuredImage: "./test.jpg"
---

### First steps

```python
print("Hi there!)
test = [1, 2,3,4 ]
for i in test:
  print(i)
```

To setup a private CA, first create the CA itself.

Then, every time you want ot issue a certificate, submit the Certificate Signing Request, and sign it with your CA key.

### Become a CA

Generate a root authority's private key:

`openssl genrsa -des3 -out myCA.key 2048`

Enter a sufficiently strong password. Actually, you should not run this command on a machine on the internet, as this will be the root of your trust. If you do this on a system that is compromised, the security chain 'breaks'.

Generate the root certificate:

`openssl req -x509 -new -nodes -key myCA.key -sha256 -days 1825 -out myCA.pem`

What this does, is take the CA private key generated in the previous command, and use it to generate a public certificate for your CA.
This .pem file will later be added to trust bundles such as KeyChain, so that your browser, if looking at this keychain will use it, and confirm a certificate by your CA is to be trusted.

Answer the questions correctly to be able to find your CA later on, and have it correctly display your information. However, what you answer here does not really matter for the process itself.

Installing the CA on machines that you want to use it

## Mac OS X

Using keychain, import the pem file generated in the step before. Double click it, and say to 'Always trust' this certificate.

Creating new certificates for your domains

Create a new private key:

openssl genrsa -out newdomain.com.key 2048

Create a new CSR and add alternative names:

`openssl req -new -key newdomain.com.key -out newdomain.com.csr`

Allow alternative names by creating a file newdomain.com.ext with contents:

```bash
    authorityKeyIdentifier=keyid,issuer
    basicConstraints=CA:FALSE
    keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
    subjectAltName = @alt_names

    [alt_names]
    DNS.1 = newdomain.com
    DNS.2 = 192.168.0.4
```

*you could add more alt names if necessary.

Sign the CSR to issue the certificate

Finally, use your CA private key to sign the csr, and issue the certificate signed by your CA:

  ```bash
  openssl x509 -req -in [newdomain.com](http://newdomain.com).csr \
  -CA myCA.pem \
  -CAkey myCA.key \
  -CAcreateserial
  -out newdomain.com.crt \
  -days 1825 -sha256 \
  -extfile newdomain.com.ext
  ```

Install the certificate on your web server

You now have a newdomain.crt, newdomain.csr, newdomain.ext and newdomain.key file.

The crt and key should be saved and, together with the CA certificate, added to your webserver:

Privatekey is the .key file,
Certificate is the .crt file,
CertificateChain is the .pem file.

Restart the webserver and your browser afterwards, as the message that the site is not trusted sometimes does not go away before doing so.

Registering custom domains on your router

To get domain names accepted on asus routers with asusWrtMerlin, follow these setup:

Note: Make sure you have enabled (Advanced Settings→Administration→System ) Enable JFFS custom scripts and configs and enabled ssh access to the router.

Next, login to the router with ssh:

Create /jffs/configs/dnsmasq.conf.add

Contents:

```bash
address=/router.home/192.168.0.4
STRING="Test"
echo ${STRING}
# ...etc.
# you can add multiple domains to the same IP by: address=/domain1.com/domain2.com/192.168.0.4
```

Then, run:  `service restart_dnsmasq`

References: https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/
