
<!--
## The why and how

It has been approximately 20 years after I 'deployed' my first website to a geocities domain after familiarizing myself with the (so much more limited) set of `<html>` tags.
Although the design got forever lost (certainly for the better..), I can still remember what it looked like with a `<table>` at the top of the page displaying my menu buttons, loading separate html pages in the Iframe below it (of
course, containing many more tables..).
The amazement of finding my first self-created website online may never really fade (even though probably no-one was ever able to find it.)

A few years later I started creating some websites in a server side language, Active Server Pages (`ASP`), which at some point evolved into a small 'community forum' where we could plan events such as going to concerts and letting each other know if we'd be there and if we could create tickets.

It's been a long time since then, and
After the head-breaking years of iframe and table layouts, and finally the evolution of modern webbrowsers and some better standardization in parsing HTML, CSS and Javascript.

And how much things have changed!
It's 2019 and creating a website finally feels 'right' again. -->
