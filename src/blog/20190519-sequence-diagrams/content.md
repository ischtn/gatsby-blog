---
title: "Sequence digrams in Markdown and Gatsbyjs"
date: "2019-04-19"
featuredImage: "./test.jpg"
status: Published
---

## Test


### Example

```mermaid
graph LR
install[Install Plugin]
install --> configure[Configure Plugin]
configure --> draw[Draw Fancy Diagrams]
```

```mermaid
sequenceDiagram
    Alice->>John: Hello John, how are you?
    John-->>Alice: Great!
```