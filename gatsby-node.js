const path = require(`path`);

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;

  if (node.internal.type === `MarkdownRemark`) {
    const slug = path.basename(path.dirname(node.fileAbsolutePath));
    const node_path = path.dirname(node.fileAbsolutePath);
    const is_blog = node_path.includes("src/blog/");

    if (is_blog) {
      createNodeField({
        node,
        name: `slug`,
        value: `/blog/${slug}`
      });
    } else {
      createNodeField({
        node,
        name: `slug`,
        value: `/project/${slug}`
      });
    }
  }
};

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const result = await graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              keywords
              excerpt
              link
              title
              status
            }
          }
        }
      }
    }
  `);

  const posts = result.data.allMarkdownRemark.edges;

  posts.forEach(({ node }, index) => {
    const prev = index === 0 ? false : posts[index - 1].node;
    const next = index === posts.length - 1 ? false : posts[index + 1].node;

    if (node.frontmatter.status !== "Published") {
      return;
    }

    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/templates/blog-post.js`),
      context: {
        slug: node.fields.slug,
        prev,
        next
      }
    });
  });
};

exports.onCreateWebpackConfig = ({ stage, actions }) => {
  if (stage === "develop") {
    actions.setWebpackConfig({
      devtool: "cheap-module-source-map"
    });
  }
};

// For react hot loader
exports.onCreateWebpackConfig = ({ getConfig, stage }) => {
  const config = getConfig();
  if (stage.startsWith("develop") && config.resolve) {
    config.resolve.alias = {
      ...config.resolve.alias,
      "react-dom": "@hot-loader/react-dom"
    };
  }
};
