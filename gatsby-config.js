module.exports = {
  siteMetadata: {
    title: `Igor Schouten`
  },
  plugins: [
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-mermaid",
            options: {
              language: "mermaid",
              theme: "neutral"
            }
          },
          `gatsby-plugin-react-helmet`,
          `gatsby-remark-relative-images`,
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 750,
              linkImagesToOriginal: false,
              backgroundColor: "none"
            }
          },
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              maxWidth: 80,
              classPrefix: "language-",
              inlineCodeMarker: null,
              // This lets you set up language aliases.  For example,
              // setting this to '{ sh: "bash" }' will let you use
              // the language "sh" which will highlight using thes
              // bash highlighter.
              aliases: {},
              // If you wish to only show line numbers on certain code blocks,
              // leave false and use the {numberLines: true} syntax below
              showLineNumbers: false,
              // If setting this to true, the parser won't handle and highlight inline
              // code used in markdown i.e. single backtick code like `this`.
              noInlineHighlight: false
            }
          },
          {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
              trackingId: "UA-113995226-1",
              anonymize: true
            }
          },
          `gatsby-transformer-sharp`,
          `gatsby-plugin-sharp`
        ]
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `posts`,
        path: `${__dirname}/src`,
        ignore: [`**/\.*`]
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `projects`,
        path: `${__dirname}/src`,
        ignore: [`**/\.*`]
      }
    }
  ]
};
